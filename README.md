### Universal boilerplate for creating libraries with Rollup, JS, TS, GCC, and Babel

[![Build Status](https://travis-ci.org/Kflash/spiky.svg?branch=master)](https://travis-ci.org/Kflash/spiky)
[![Circle CI](https://circleci.com/gh/Kflash/spiky/tree/master.svg?style=svg)](https://circleci.com/gh/Kflash/spiky/tree/master)
[![Build Status](https://semaphoreci.com/api/v1/trixtor/spiky/branches/master/badge.svg)](https://semaphoreci.com/trixtor/spiky)
[![Coverage Status](https://coveralls.io/repos/github/Kflash/spiky/badge.svg?branch=master)](https://coveralls.io/github/Kflash/spiky?branch=master)
[![codecov](https://codecov.io/gh/Kflash/spiky/branch/master/graph/badge.svg)](https://codecov.io/gh/Kflash/spiky)
[![npm version](https://badge.fury.io/js/spiky.svg)](https://badge.fury.io/js/spiky)
[![npm downloads](https://img.shields.io/npm/dm/spiky.svg)](https://www.npmjs.org/package/spiky)
[![npm](https://img.shields.io/npm/l/express.svg?style=flat-square)](https://github.com/kflash/spiky/blob/master/LICENSE.md)

> The next generation Boily ES2015 boilerplate

## Features

* [ES6 with Babel](http://babeljs.io/) as the ES6 and ES7 compiler
* [Rollup](http://rollupjs.org/) for bundling ES6 down to CommonJS, ES2015 and UMD
* [Eslint](http://eslint.org/) to maintain a consistent code style
* [Karma](http://karma-runner.github.io/0.13/index.html) as the test runner
* [Mocha](http://mochajs.org/)-[Chai](http://chaijs.com/)-[Sinon](http://sinonjs.org/) pre-configured testing stack
* [Universal Module Definition (UMD) API](https://github.com/umdjs/umd), which provides compatibility with the most popular script loaders, to the output.
* [Gulp](http://gulpjs.com/) as the streaming build system.
* [Livereload](http://livereload.com/) an implementation of the LiveReload server in the `Node.js environment`.
* [Istanbul](https://github.com/gotwarlost/istanbul) and [Coveralls](https://coveralls.io/) as the pre-configured test coverage
* [TypeScript](https://www.typescriptlang.org/) support for bundling Angular 2 and similiar projects
* Cross-platform, minimum dependencies
* Setup to work with both Travis and Circle
* Node >= 5.x

## Quick start

The only development dependency of this project is [`Node.js`](https://nodejs.org/en/). So just make sure you have it installed. Then type few commands known to every Node developer...

```js
$ git clone https://github.com/kflash/spiky.git
$ cd spiky
$ npm install                   # Install Node modules listed in ./package.json
$ npm run build                 # Build a minified and a non-minified version of the library
```

... and boom! You have it all setup for you!

## Workflow

* `npm run dependencies` - List installed packages
* `npm run dependencies:purge` - Remove all dependencies
* `npm run dependencies:reinstall` - Reinstall all dependencies
* `npm run dependencies:check` - shows a list over dependencies with a higher version number then the current one - if any
* `npm run dependencies:upgrade` - automatically upgrade all devDependencies & dependencies, and update package.json

## Gulp tasks

* `gulp build` - build task that generates both minified and non-minified scripts
* `gulp build:dev` - build task that generate a non-minified script
* `gulp build:prod` - build task that generate a minified script
* `gulp build:cjs` - Build task that generate a CommonJS bundle
* `gulp build:es6` - Build task that generate a ES2015 bundle
* `gulp build:ts` - Build task that generate a ES5 bundle from TypeScript
* `gulp lint:src` - lint the source
* `gulp lint:ts` - lint the TypeScript source files
* `gulp lint:tests` - lint the unit tests
* `gulp clean` - remove the coverage report - and the *dist* folder
* `gulp node:cov` - run Isparta, a code coverage tool
* `gulp test` - runs unit tests for the browser and the `Node.js environment`
* `gulp test:browser` - runs the unit tests for browser
* `gulp test:chrome` - runs the unit tests for browser with Chrome
* `gulp test:phantom` - runs the unit tests for browser with PhantomJS
* `gulp test:node` - runs the unit tests in a `Node.js environment`
* `gulp watch` - run all unit tests for browser with Chrome and PhantomJS, and watch files for changes
* `gulp watch:node` - run all unit tests in the node environemnt, and watch files for changes
* `gulp watch:browser` - run all unit tests for browser and watch files for changes
* `gulp watch:chrome` - run all unit tests for browser with Chrome and watch files for changes
* `gulp watch:phantom` - run all unit tests for browser with PhantomJS and watch files for changes
* `gulp prepare:ts` - grab all TypeScript test files and bundle them down to ES5 for use with unit tests
* `gulp compile:closure` - Convert JS down to Google Closure
* `gulp build:closure` - Build with Google Closure compiler
* `gulp gzip` - Gzip all files in the `~../dist/` folder
* `gulp tarball` - Create a tarball from files inside the `~../dist/` folder
* `gulp bump` - Bump `package.json` version number, and auto-commit your latest changes to a .git account

## Multi bundling

Spiky has a couple of built-in tools that allow you to bundle different packages at once. E.g. take a look at the `./config/gulp/build-common.js` file as a good example.

All you need to do is to import the bundle from the `util folder`, and  call it with two arguments - format and the entry point. E.g. `bundle('cmd', '/foo/index.js);`

## Testing environment

This project uses `Mocha` to run your unit tests, it uses `Karma` as the test runner, it enables the feature that you are able to render your tests to the browser (e.g: Firefox, Chrome etc.).

To add a unit test, simply create a `.spec.browser.js` or a `.spec.node.js` file inside the `~../test/specs/` folder. Karma will pick up on these files automatically, and Mocha and Chai will be available within your unit tests without the need to import them.

To keep watching the common test suites that you are working on, simply do `gulp watch:browser` or `gulp watch:node`.

## Browser tests

The browser spec runner - `./config.runner.html` - can be opened in a browser to run your tests. For it to work, you must first run `gulp browser`. This will set up a watch task that will automatically refresh the tests when your scripts, or the tests, change.

The browser tests are setup to work both with the browser and `Node.js environment` unit tests.

## Coverage reports

This boilerplate are setup with two different code coverage reports - one for `Karma` and one for the `Node.js environment`.
By default your coverage report for the `Node.js environment` will be published automatically to `coveralls.io` webpage if you have created an account there.

## Dependency management

Spiky has build-in some nice features to always make sure your dependency tree are up to date.

To check for the latest dependencies, simply run `npm run dependencies:check`.

If you want to update your packages, you can simply do npm run `dependencies:upgrade`.

**Note!** Your package.json will be automatically updated so make sure you have saved your changes before running this.

To reinstall all packages, do npm run `dependencies:reinstall`, and to remove all packages npm run package:purge.


## FAQ

### Does this boilerplate work with Angular 2?

Should work just fine. Spiky let you compile `TypeScript` down to ES6 / ES5. This also works for the unit tests.

There are a few Gulp taks tasks dedicated to `TypeScript`.

### How do I make a TypeScript bundle, and run unit tests?

Put your `.ts` files in the source folder, and then `gulp build:ts`.

For unit tests, it will work the same way. Put your `.ts` file in the  - `./test` folder and run the tests.

It is also a Gulp task -  `gulp prepare:ts` - that make a ES5 bundle out of all your unit tests.

You can lint your TS source files with `gulp lint:ts`.

### Does this boilerplate works with the `Bublé ES2015 compiler`?

Yes. This will work with the [`Bublé`](https://gitlab.com/Rich-Harris/buble). Even TypeScript will work but there are no support for Flow. This is
rather a Rollup issue then a `Bublé` issue.

### How to set this up to work with `Bublé`

You can get inspiration from one of my other boilerplates - [`Farris`](https://github.com/Kflash/farris)

### Is the provided tools only for Github?

Yes. E.g `gulp bump` behave different on `GitLab` and `Bitbucket` and will not work. This is a plug-in issue, not an boilerplate issue.

### When should I consider using this boilerplate?

This library is ideal for libraries that export a single file.

### Why isn't a sourcemap generated for the bundle?

Because this is not set by default. It's very simple to add it. This boilerplate are using
[`rollup-stream`](https://www.npmjs.com/package/rollup-stream), so simply follow `example #2` in the readme on their Github repo to set it up.

### What's the browser compatibility?

As a rule of thumb, Babel works best in IE9 and above.

### What's the cost of transpiling?

A thorough analysis of this question can be found [here](https://github.com/samccone/The-cost-of-transpiling-es2015-in-2016).

# Customizing

This boilerplate is very customizable. Update the value of `name` in the `package.json` file to change the name of the variable exported from this boilerplate. To make other changes,
look at the `Gulp` tasks in the `./config/gulp/task folder`.

There are a few tools located inside `./config/gulp/util folder` which you can use to configure this boilerplate.

E.g. `commit(msg)` to be used to commit any changes to a `.git` repo and `push(origin, master)` to push commits.

`gulp bump` uses both this two tools. Take a look at how it's done :-)

# License

The MIT License (MIT)

Copyright (c) 2016 KFlash

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
