import lintConf from '../../ts/tslint.json';

/**
 * Returns the linting configuration to be used for `codelyzer`.
 * @return {string[]} the list of linting rules
 */
function customRules() {
	return lintConf.rulesDirectory;
}

export default customRules;