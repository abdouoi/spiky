import gulp from 'gulp';
import runSequence from 'run-sequence';
import sequenceComplete from '../util/sequenceComplete';

gulp.task('watch', done => {
	runSequence(
		['watch:phantom', 'watch:chrome'],
		sequenceComplete(done));
});