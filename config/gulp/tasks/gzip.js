import gulp from 'gulp';
import gZip from 'gulp-gzip';

gulp.task('gzip', () => {
	gulp.src('./dist/*.js')
		.pipe(gZip({ threshold: 1024 }))
		.pipe(gulp.dest('./dist'));
});